# AR-gestützte Schnitzeljagd

## Träger des Projekts
Stadt Osnabrück
Jan Uhlenbrok / Projektleitung Smart Region Osnabrück / uhlenbrok@osnabrueck.de

## Wunsch nach weiteren Kooperationspartnern	
Nein

## Projektstatus
Konzept

## Beschreibung
Die AR-gestützte Schnitzeljagd soll spielerisch die Thematik AR näherbringen, kann aber grundsätzlich auch für anderen Fälle eingesetzt werden.
Spieler:innen müssen an bestimmten Punkten (z.B. in der Stadt) einen versteckten Code finden. Scannen sie diesen mit dem Smartphone, so erscheint auf dem Bildschirm ein 3D-Objekt, welches frei beweglich "im Raum" schwebt. Auf diesem Objekt finden sich Hinweise für die Lösung eines Rätsels bzw. Hinweise auf den Standort des nächsten Codes.
Das Spielprinzip ist fix, die Inhalte (Texte, Bilder, 3D-Modelle) sind aber austauschbar, so dass beliebige Einsatzmöglichkeiten denkbar sind.
Es muss keine App installiert werden, das Spiel ist komplett im Browser lauffähig. Der Code basiert auf PHP und JavaScript.

## Zeitplan
Die prototypische Umsetzung erfolgt in Q1 2023. Finalisierung Q2 2023.

## Weiterführende Links
